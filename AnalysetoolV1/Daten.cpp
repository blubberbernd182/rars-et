/**
* @file Daten.cpp
* @brief Implementierungsdatei der Klasse "Daten"
* @date 17.12.2013
* @author Alexander Denner, Benjamin Haas
**/
#include "Daten.h"
using namespace std;


/**
* @brief Konstruktor von "Daten"
* @param keine
* @return keine
**/
Daten::Daten ()
{
   nAnz_reihen = 0;
   nAnz_rennen = 0;
   nAnz_runden = 0;
   nStart_naechste = 0;
   nStartid.clear ();
   nEndid.clear ();
}


/**
* @brief Erfassung der Dauer eines Rennens
* @param [in] int p_nIndex: Index des Rennbereichs in der Datenbank
* @return maximale Renndauer
**/
double Daten::gib_dauer (int p_nIndex)
{
   mysqlpp::Query query = conn.query ();
   query << "SELECT MAX(racetime) AS t_max FROM tmtrace WHERE robot = " << mysqlpp::quote << sName << " AND id BETWEEN " << nStartid [p_nIndex]  << " AND " << nEndid [p_nIndex] << ";";
   res = query.store ();
   return res[0]["t_max"];
}


/**
* @brief Erfassung der Durchschnittsgeschwindigkeit eines Rennens
* @param [in] int p_nIndex: Index des Rennbereichs in der Datenbank
* @return durchschnittliche Geschwindigkeit
**/
double Daten::gib_durchschnitt_v (int p_nIndex)
{
   mysqlpp::Query query = conn.query ();
   query << "SELECT AVG(velocity) AS v_avg FROM tmtrace WHERE robot = " << mysqlpp::quote << sName << "AND id BETWEEN " << nStartid [p_nIndex]  << " AND " << nEndid [p_nIndex] << ";";
   res = query.store ();
   return res [0]["v_avg"];
}


/**
* @brief Erfassung des maximalen Schadens eines Rennens
* @param [in] int p_nIndex: Index des Rennbereichs in der Datenbank
* @return maximaler Schaden
**/
double Daten::gib_max_schaden (int p_nIndex)
{
   mysqlpp::Query query = conn.query ();
   query << "SELECT MAX(damage) AS d_max FROM tmtrace WHERE robot = " << mysqlpp::quote << sName << " AND id BETWEEN " << nStartid [p_nIndex]  << " AND " << nEndid [p_nIndex] << ";";
   res = query.store ();
   return res [0]["d_max"];
}


/**
* @brief Erfassung der maximalen Geschwindigkeit eines Rennens
* @param [in] int p_nIndex: Index des Rennbereichs in der Datenbank
* @return maximale Geschwindigkeit
**/
double Daten::gib_max_v (int p_nIndex)
{
   mysqlpp::Query query = conn.query ();
   query << "SELECT MAX(velocity) AS v_max FROM tmtrace WHERE robot = " << mysqlpp::quote << sName << "AND id BETWEEN " << nStartid [p_nIndex]  << " AND " << nEndid [p_nIndex] << ";";
   res = query.store ();
   return res [0]["v_max"];
}


/**
* @brief Rückgabe der Anzahl an Rennen
* @param keine
* @return Anzahl an Rennen, abhängig von der Auswahl im Hauptmenü ("Letztes Rennen", "Suche Strecke" oder "Alle Rennen")
**/
int Daten::gib_rennen () const
{
   return nAnz_rennen;
}


/**
* @brief Erfassung der Startzeit eines Rennens
* @param [in] int p_nIndex: Index des Rennbereichs in der Datenbank
* @return Startzeit
**/
string Daten::gib_startzeit (int p_nIndex)
{
   mysqlpp::Query query = conn.query ();
   query << "SELECT tmtime FROM tmtrace WHERE robot = " << mysqlpp::quote << sName << " AND id = " << nStartid [p_nIndex] << ";";
   res = query.store ();
   sStartzeit = mysqlpp::stream2string (res [0]["tmtime"]);
   sStartzeit.insert (10, ",");
   return sStartzeit;
}


/**
* @brief Erfassung des Streckennamens für ein Rennen
* @param [in] int p_nIndex: Index des Rennbereichs in der Datenbank
* @return Streckenname (ohne trk-Endung)
**/
string Daten::gib_strecke (int p_nIndex)
{
   mysqlpp::Query query = conn.query ();
   query << "SELECT userdata FROM tmtrace WHERE robot = " << mysqlpp::quote << sName << " AND id = " << nStartid [p_nIndex]  << ";";
   res = query.store ();

   sStrecke = mysqlpp::stream2string (res [0]["userdata"]);
   sStrecke.erase (sStrecke.end () - 4, sStrecke.end ());     //Löschen der letzten vier Stellen des Ergebnisstrings

   if (sStrecke == "hock")
   {
      return "hockenheim (hock)";
   }
   else
   {
      if (sStrecke == "silverst")
      {
         return "silverstone (silverst)";
      }
      else
      {
         return sStrecke;
      }
   }
   return sStrecke;
}


/**
* @brief Erfassung der Start- und Endids eines Rennbereichs
* @param [in] int p_nArt: Vorbelegung = 0; Unterscheidungsvariable, wie Grenzen ausgelesen werden; 1: Letztes Rennen, 2: Strecke suchen, 0: Alle Rennen
* @param [in] string p_sEingabe: Vorbelegung = "."; Streckennamen, nach dem gesucht werden soll
* @return keine
**/
void Daten::erfasse_grenzen (int p_nArt, string p_sEingabe)
{
   mysqlpp::Query query = conn.query ();

   nStartid.clear ();     //Elemente in nStartid[] und nEndid[] löschen
   nEndid.clear ();

   sStrecke = trk_anhaengen (p_sEingabe);     //an eingegebene Strecke (p_sEingabe) wird ".trk"-Endung angehängt

   switch (p_nArt)
   {
      case 1:     //Menüpunkt 1: Letztes Rennen
      {
         nAnz_rennen = 1;
         //Startid bestimmen
         query << "SELECT id FROM tmtrace WHERE robot = " << mysqlpp::quote << sName << " AND id = (SELECT MAX(id) FROM tmtrace WHERE racetime = 0 AND robot = " << mysqlpp::quote << sName <<");";
         res = query.store ();
         nStartid.push_back (res [0]["id"]);
         //Endid bestimmen
         query << "SELECT MAX(id) as id_max from tmtrace where robot = " << mysqlpp::quote << sName << ";";
         res = query.store ();
         nEndid.push_back (res [0]["id_max"]);
         break;
      }
      default:  //Menüpunkt 2 und 3: Suche_Strecke bzw. Alle_Rennen
      {
         //Startids bestimmen
         query << "SELECT id FROM tmtrace WHERE racetime = 0 AND robot = " << mysqlpp::quote << sName;

         if (p_nArt == 2)     //falls "Suche Strecke" ausgewählt: im SQL-Query zusätzlich nach Strecke filtern
         {
            query << " AND userdata = " << mysqlpp::quote << sStrecke;
         }
         query << " ORDER BY id DESC;";
         res_0 = query.store ();
         nAnz_reihen =  res_0.num_rows ();     //Anzahl an Startids in der Datenbank speichern


         for (size_t i = 0; i < nAnz_reihen; i++)
         {
            nStartid.push_back (res_0 [i]["id"]);     //Startids speichern

            //nächst höhere Startid erfassen: kleinste ID in Datenbank, die größer als nStartid[] ist, sowie racetime = 0
            query << "SELECT MIN(id) AS id_min FROM tmtrace WHERE racetime = 0 AND robot = " << mysqlpp::quote << sName << " AND id > " << mysqlpp::quote << nStartid[i] << ";";
            res = query.store();
            if (res[0]["id_min"].is_null())     //Ergebnis ist leer: nach diesem Streckenbereich folgt kein neuer mehr, d.h. Endid ist die max. ID in Datenbank
            {
               query << "SELECT MAX(id) AS id_max FROM tmtrace WHERE robot = " << mysqlpp::quote << sName;
               if (p_nArt == 2)
               {
                  query << " AND userdata = " <<mysqlpp::quote << sStrecke;
               }
               res = query.store ();
               nEndid.push_back (res [0]["id_max"]);
            }
            else
            {
               nStart_naechste = res [0]["id_min"];     //nächst höhere Startid zwischenspeichern
               //Endid bestimmen: Maximale ID im Bereich (nStartid[] - nStart_naechste)
               query << "SELECT MAX(id) as id_max FROM tmtrace WHERE robot = " << mysqlpp::quote << sName << " AND id >= " << mysqlpp::quote << nStartid [i] << " AND id < " << mysqlpp::quote << nStart_naechste;
               if (p_nArt == 2)
               {
                  query << " AND userdata = " << mysqlpp::quote << sStrecke;
               }
               res = query.store ();
               nEndid.push_back (res[0]["id_max"]);
            }
         }
         nAnz_rennen = nAnz_reihen;
         break;
      }
   }
}


/**
* @brief empfängt das Connection-Objekt aus "Ausgabe"
* @param [in,out] mysqlpp::Connection pConn: Connection-Objekt
* @return keine
**/
void Daten::setze_connection (mysqlpp::Connection &pConn)
{
   conn = pConn;
}


/**
* @brief setzt den Namen des Robot
* @param [in] string p_sName: Name des Robots
* @return keine
**/
void Daten::setze_name (string p_sName)
{
   sName = p_sName;
}


/**
* @brief hängt zur weiteren Verarbeitung der eingegebenen Strecke bei Menüpunkt 2 die Endung ".trk" an
* @param [in] string p_sEingabe: eingegebene Strecke
* @return eingegebene Strecke mit Endung ".trk"
**/
string Daten::trk_anhaengen (string p_sEingabe) const
{
   if (p_sEingabe == "hockenheim")
   {
      p_sEingabe = "hock.trk";
   }
   else
   {
      if (p_sEingabe == "silverstone")
      {
         p_sEingabe = "silverst.trk";
      }
      else
      {
         p_sEingabe += ".trk";
      }
   }
   return p_sEingabe;
}
