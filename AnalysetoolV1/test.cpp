/**
* @file test.cpp
* @brief Datei, welche die Routine zum Testen der Klasse "Daten" enthält
* @date 17.12.2013
* @author Alexander Denner, Benjamin Haas
**/

//Diese Testroutine wurde entwickelt, um die korrekte Funktion der Methoden der Klasse "Daten" zu überprüfen. Vor dem Ausführen des Programms muss die Tabelle "tmtrace"
//der Datenbank gelöscht werden, da sie hier mit definierten Werten beschrieben wird, um nachher die Richtigkeit der Ausgaben zu überprüfen.
//Das Programm funktioniert wie folgt: Es werden zunächst bestimmte Werte in die Datenbank geschrieben. Anschließend werden die Methoden der Klasse Daten
//aufgerufen und deren Ergebnisse mit den Soll-Werten verglichen. Die Anzahl der gefundenen Fehler wird anschließend in der Konsole ausgegeben.

#include "Daten.h"
#include <iostream>
#include <string>
#include <math.h>


/**
* @brief Beschreibt die Tabelle tmtrace der Rars-DB mit definierten Werten dreier Beispielrennen.
* @return keine
**/
void Datenbank_beschreiben (mysqlpp::Connection &pConn)
{
   mysqlpp::Query query = pConn.query ();

   //Rennen auf silverst.trk mit folgenden Daten
   //maximale Geschwindigkeit: 18
   //Duŕchschnittsgeschwindigkeit: 15
   //ḿaximaler Schaden: 0
   //Dauer: 100

   query << "INSERT INTO tmtrace (robot, starttime, racetime, laps, velocity, fuel, damage, userdata) VALUES ("
         << "'Robot32', 0.1, 0, 0, 10, 150, 0, 'silverst.trk')";
   query.execute ();
   query << "INSERT INTO tmtrace (robot, starttime, racetime, laps, velocity, fuel, damage, userdata) VALUES ("
         << "'Robot32', 0.1, 10, 5, 17, 140, 0, 'silverst.trk')";
   query.execute ();
   query << "INSERT INTO tmtrace (robot, starttime, racetime, laps, velocity, fuel, damage, userdata) VALUES ("
         << "'Robot32', 0.1, 100,10, 18, 130, 0, 'silverst.trk')";
   query.execute ();

   //Eine Zeile eines anderen Robots zur Verwirrung
   query << "INSERT INTO tmtrace (robot, starttime, racetime, laps, velocity, fuel, damage, userdata) VALUES ("
         << "'Robot18', 1.5, 50, 200, 5, 2493, 0, 'hock.trk')";
   query.execute ();

   //noch ein Rennen auf hock.trk mit folgenden Daten
   //maximale Geschwindigkeit: 33,897
   //Durchschnittsgeschwindigkeit: 11,829
   //maximaler Schaden: 2
   //Dauer: 42

   query << "INSERT INTO tmtrace (robot, starttime, racetime, laps, velocity, fuel, damage, userdata) VALUES ("
         << "'Robot32', 1.5, 0, 0, 0, 150, 0, 'hock.trk')";
   query.execute ();
   query << "INSERT INTO tmtrace (robot, starttime, racetime, laps, velocity, fuel, damage, userdata) VALUES ("
         << "'Robot32', 1.5, 10, 1, 33.897, 100, 0, 'hock.trk')";
   query.execute ();
   query << "INSERT INTO tmtrace (robot, starttime, racetime, laps, velocity, fuel, damage, userdata) VALUES ("
         << "'Robot32', 1.5, 42, 385, 1.59, 99, 2, 'hock.trk')";
   query.execute ();

   //Rennen auf hock.trk mit folgenden Daten
   //maximale Geschwindigkeit: 100
   //Durchschnittsgeschwindigkeit: 50
   //maximaler Schaden: 17
   //Dauer: 6

   query << "INSERT INTO tmtrace (robot, starttime, racetime, laps, velocity, fuel, damage, userdata) VALUES ("
         << "'Robot32', 0, 0, 0, 0, 150, 0, 'hock.trk')";
   query.execute ();
   query << "INSERT INTO tmtrace (robot, starttime, racetime, laps, velocity, fuel, damage, userdata) VALUES ("
         << "'Robot32', 0, 2, 0, 100, 140, 5, 'hock.trk')";
   query.execute ();
   query << "INSERT INTO tmtrace (robot, starttime, racetime, laps, velocity, fuel, damage, userdata) VALUES ("
         << "'Robot32', 0, 6, 1, 50, 110, 17, 'hock.trk')";
   query.execute ();
}


/**
* @brief Vergleicht den Inhalt von 2 double-Arrays der Länge 4 und 2 Strings.
* @param [in] double p_dSoll[3]: Array1 der Länge 4
* @param [in] double p_dIst[3]: Array2 der Länge 4
* @param [in] std::string p_sSollString: String1
* @param [in] std::string p_sIstString: String 2
* @return Die Anzahl der Unterscheidungen
**/
int Ergebnise_ueberpruefen(double p_dSoll[3], double p_dIst[3], std::string p_sSollString, std::string p_sIstString)
{
   int nFehler = 0;     //Anzahl gefundener Fehler
   for(int i = 0; i < 4; i++)     //Feldindex durchlaufen
   {
      double dDiff = fabs(p_dSoll[i] - p_dIst[i]);     //Differenz zwischen Soll- und IstWert
      if (! (dDiff < 0.0001))     //Auf Gleichheit der beiden Werte prüfen
         nFehler += 1;     //Fehleranzahl um eins erhöhen
   }
   if (! p_sSollString.compare (p_sIstString))
      nFehler += 1;     //Fehleranzahl um eins erhöhen
   return nFehler;     //Anzahl gefundener Fehler zurückgeben
}


/**
* @brief Hauptfunktion des Programms zum Testen der Klasse Daten
* @param keine
* @return -1 bei Fehler, sonst 0
**/
int main()
{
   mysqlpp::Connection conn (false);
   if (!conn.connect ("rars", "localhost", "rars", "r2a0c1e4"))
   {
      cout << "Verbindung zur Datenbank fehlgeschlagen!" << endl;     //Fehlermeldung wenn Verbindung fehlgeschlagen
      return -1;
   }

   Datenbank_beschreiben (conn);     //Testwerte in Datenbank schreiben

   Daten data;     //Objekt der Klasse Daten erstellen
   data.setze_connection (conn);     //Dem Objekt von Daten eine mysql-Verbindung übergeben.
   data.setze_name ("Robot32");     //Name des Robots angeben

   double nSollwerte[4] = {0};     //Array für Soll-Werte
   double nIstwerte[4] = {0};     //Array für Ist-Werte
   std::string sSollstrecke;     //String für Soll-Wert der Strecke
   std::string sIststrecke;     //String für Ist-Wert der Strecke
   int nFehler_gesamt = 0;     //Anzahl an Fehler

   //Modus letztes Rennen
   data.erfasse_grenzen (1, "hock");

   nSollwerte[0] = 100;     //Maximale Geschwindigkeit des ersten Rennens
   nSollwerte[1] = 50;     //Durchschnittsgeschwindigkeit des ersten Rennens
   nSollwerte[2] = 17;     //Maximaler Schaden des ersten Rennens
   nSollwerte[3] = 6;     //Dauer des ersten Rennens
   sSollstrecke = "hock.trk";

   nIstwerte[0] = data.gib_max_v(0);     //Maximale Geschwindigkeit des letzten Rennens aus Abfrage
   nIstwerte[1] = data.gib_durchschnitt_v(0);     //Durchschnittsgeschwindigkeit des letzten Rennens aus Abfrage
   nIstwerte[2] = data.gib_max_schaden(0);     //Maximaler Schaden des letzten Rennens aus Abfrage
   nIstwerte[3] = data.gib_dauer(0);     //Dauer des letzten Rennens aus Abfrage
   sIststrecke = data.gib_strecke(0);     //Strecke des letzten Rennens aus Abfrage
   nFehler_gesamt += Ergebnise_ueberpruefen(nSollwerte, nIstwerte, sSollstrecke, sIststrecke);

   //Modus suche Strecke
   data.erfasse_grenzen (2, "hock");

      //Erstes Rennen wie oben
      nIstwerte[0] = data.gib_max_v(0);     //Maximale Geschwindigkeit des ersten Rennens aus Abfrage
      nIstwerte[1] = data.gib_durchschnitt_v(0);     //Durchschnittsgeschwindigkeit des ersten Rennens aus Abfrage
      nIstwerte[2] = data.gib_max_schaden(0);     //Maximaler Schaden des ersten Rennens aus Abfrage
      nIstwerte[3] = data.gib_dauer(0);     //Dauer des ersten Rennens aus Abfrage
      sIststrecke = data.gib_strecke(0);     //Strecke des ersten Rennens aus Abfrage
      nFehler_gesamt += Ergebnise_ueberpruefen(nSollwerte, nIstwerte, sSollstrecke, sIststrecke);

       //zweites Rennen neue Sollwerte
      nSollwerte[0] = 33.897;     //Maximale Geschwindigkeit des ersten Rennens
      nSollwerte[1] = 11.829;     //Durchschnittsgeschwindigkeit des ersten Rennens
      nSollwerte[2] = 2;     //Maximaler Schaden des ersten Rennens
      nSollwerte[3] = 42;     //Dauer des ersten Rennens
      sSollstrecke = "hock.trk";     //Strecke des ersten Rennens
      nIstwerte[0] = data.gib_max_v(1);     //Maximale Geschwindigkeit des zweiten Rennens aus Abfrage
      nIstwerte[1] = data.gib_durchschnitt_v(1);     //Durchschnittsgeschwindigkeit des zweiten Rennens aus Abfrage
      nIstwerte[2] = data.gib_max_schaden(1);     //Maximaler Schaden des zweiten Rennens aus Abfrage
      nIstwerte[3] = data.gib_dauer(1);     //Dauer des zweiten Rennens aus Abfrage
      sIststrecke = data.gib_strecke(1);     //Strecke des zweiten Rennens aus Abfrage
      nFehler_gesamt += Ergebnise_ueberpruefen(nSollwerte, nIstwerte, sSollstrecke, sIststrecke);

   //Modus alle Rennen
   data.erfasse_grenzen (0);

      //Erstes Rennen
      nSollwerte[0] = 100;     //Maximale Geschwindigkeit des ersten Rennens
      nSollwerte[1] = 50;     //Durchschnittsgeschwindigkeit des ersten Rennens
      nSollwerte[2] = 17;     //Maximaler Schaden des ersten Rennens
      nSollwerte[3] = 6;     //Dauer des ersten Rennens
      sSollstrecke = "hock.trk";     //Strecke des ersten Rennens
      nIstwerte[0] = data.gib_max_v(0);     //Maximale Geschwindigkeit des ersten Rennens aus Abfrage
      nIstwerte[1] = data.gib_durchschnitt_v(0);     //Durchschnittsgeschwindigkeit des ersten Rennens aus Abfrage
      nIstwerte[2] = data.gib_max_schaden(0);     //Maximaler Schaden des ersten Rennens aus Abfrage
      nIstwerte[3] = data.gib_dauer(0);     //Dauer des ersten Rennens aus Abfrage
      sIststrecke = data.gib_strecke(0);     //Strecke des ersten Rennens aus Abfrage
      nFehler_gesamt += Ergebnise_ueberpruefen(nSollwerte, nIstwerte, sSollstrecke, sIststrecke);

      //Zweites Rennen
      nSollwerte[0] = 33.897;     //Maximale Geschwindigkeit des zweiten Rennens
      nSollwerte[1] = 11.829;     //Durchschnittsgeschwindigkeit des zweiten Rennens
      nSollwerte[2] = 2;     //Maximaler Schaden des zweiten Rennens
      nSollwerte[3] = 42;     //Dauer des zweiten Rennens
      sSollstrecke = "hock.trk";     //Strecke des zweiten Rennens
      nIstwerte[0] = data.gib_max_v(1);     //Maximale Geschwindigkeit des zweiten Rennens aus Abfrage
      nIstwerte[1] = data.gib_durchschnitt_v(1);     //Durchschnittsgeschwindigkeit des zweiten Rennens aus Abfrage
      nIstwerte[2] = data.gib_max_schaden(1);     //Maximaler Schaden des zweiten Rennens aus Abfrage
      nIstwerte[3] = data.gib_dauer(1);     //Dauer des zweiten Rennens aus Abfrage
      sIststrecke = data.gib_strecke(1);     //Strecke des zweiten Rennens aus Abfrage
      nFehler_gesamt += Ergebnise_ueberpruefen(nSollwerte, nIstwerte, sSollstrecke, sIststrecke);

      //Drittes Rennen
      nSollwerte[0] = 18;     //Maximale Geschwindigkeit des dritten Rennens
      nSollwerte[1] = 15;     //Durchschnittsgeschwindigkeit des dritten Rennens
      nSollwerte[2] = 0;     //Maximaler Schaden des dritten Rennens
      nSollwerte[3] = 100;     //Dauer des dritten Rennens
      sSollstrecke = "hock.trk";     //Strecke des dritten Rennens
      nIstwerte[0] = data.gib_max_v(2);     //Maximale Geschwindigkeit des dritten Rennens aus Abfrage
      nIstwerte[1] = data.gib_durchschnitt_v(2);     //Durchschnittsgeschwindigkeit des dritten Rennens aus Abfrage
      nIstwerte[2] = data.gib_max_schaden(2);     //Maximaler Schaden des dritten Rennens aus Abfrage
      nIstwerte[3] = data.gib_dauer(2);     //Dauer des drittenn Rennens aus Abfrage
      sIststrecke = data.gib_strecke(2);     //Strecke des dritten Rennens aus Abfrage
      nFehler_gesamt += Ergebnise_ueberpruefen(nSollwerte, nIstwerte, sSollstrecke, sIststrecke);

   cout << "Anzahl gefundener Fehler: " << nFehler_gesamt << endl;     //Anzahl Fehler ausgeben
   return 0;
}

