/**
* @file Ausgabe.h
* @brief Header der Klasse "Ausgabe"
* @date 17.12.2013
* @author Alexander Denner, Benjamin Haas
**/
#ifndef _AUSGABE_H
#define _AUSGABE_H

#include "Daten.h"


/**
 *  @brief Analysetool: zuständig für die Darstellung der Informationen
 */
class Ausgabe
{

public:

   Ausgabe ();

private:

   Daten data;     /**< Objekt der Klasse "Daten" */
   int nAnz_rennen;     /**< Anzahl der auszugebenden Rennen */
   int nEingabe;     /**< Eingabevariable */
   string sEingabe;     /**< Eingabevariable */

public:

   int hauptmenue ();

private:

   void ausgabe_daten ();
   void startbildschirm () const;
   bool verbindung_datenbank ();
};

#endif /*_AUSGABE_H*/
