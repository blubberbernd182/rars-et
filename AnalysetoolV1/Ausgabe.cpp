/**
* @file Ausgabe.cpp
* @brief Implementierungsdatei der Klasse "Ausgabe"
* @date 17.12.2013
* @author Alexander Denner, Benjamin Haas
**/
#include "Ausgabe.h"


/**
* @brief Konstruktor von "Ausgabe"
* @param keine
* @return keine
**/
Ausgabe::Ausgabe ()
{
   data.setze_name ("Robot32");     //einmaliges Setzen des Robotnamen
   nEingabe = -1;
   nAnz_rennen = 0;
}


/**
* @brief Benutzerführung durch das Programm
* @param keine
* @return -1: Verbindung zur Datenbank fehlgeschlagen
**/
int Ausgabe::hauptmenue ()
{
   if (verbindung_datenbank ())
   {
      cout << "Verbindung zur Datenbank ist fehlgeschlagen." << endl;
      return -1;
   }

   do
   {
      startbildschirm ();     //Kopfzeile einblenden

      cout  << "HAUPTMENÜ:" << endl
            << "----------" << endl
            << "(1) Letztes Rennen" << endl
            << "(2) Nach Strecke suchen" << endl
            << "(3) Liste aller Rennen" << endl;
      cin >> nEingabe;

      switch (nEingabe)
      {
         case 1:     //Letztes Rennen
         {
            startbildschirm ();
            data.erfasse_grenzen (1);

            cout  << "LETZTES RENNEN" << endl
                  << "--------------" << endl;
            ausgabe_daten ();
            break;
         }
         case 2:     //Strecke suchen
         {
            startbildschirm ();

            cout << "Strecke eingeben (ohne Dateiendung .trk): " << endl;
            cin >> sEingabe;
            cout << endl;

            data.erfasse_grenzen (2, sEingabe);
            ausgabe_daten ();
            break;
         }
         case 3:     //Alle Rennen
         {
            startbildschirm ();
            data.erfasse_grenzen ();
            ausgabe_daten ();
            break;
         }
         default:
         {
            break;
         }
      }
      cout << endl << "Mit beliebiger Eingabe ins Hauptmenü zurückkehren." << endl;
      if (cin.failbit) //bei Eingabe von Buchstaben
      {
         cin.clear ();
      }
      cin.get ();
      cin.ignore ();
   }
   while (1);
}


/**
* @brief formatierte Ausgabe der Renndaten auf den Bildschirm
* @param  keine
* @return keine
**/
void Ausgabe::ausgabe_daten ()
{
   nAnz_rennen = data.gib_rennen ();
   switch (nAnz_rennen)
   {
      case 0:
      {
         cout << "Kein Rennen gefunden" << endl;
         break;
      }

      case 1:     //Anzahl Rennen = 1
      {
         cout  << "Startzeit: \t\t\t"                     << data.gib_startzeit (0)<<endl
               << "Maximalgeschwindigkeit: \t"        << data.gib_max_v (0) << " mph"  << endl
               << "Durchschnittsgeschwindigkeit: \t"  << data.gib_durchschnitt_v (0) << " mph"  << endl
               << "Maximaler Schaden: \t\t"             << data.gib_max_schaden (0)       << endl
               << "Dauer des Rennens: \t\t"             << data.gib_dauer (0) << " s"   << endl
               << "Rennstrecke: \t\t\t"                   << data.gib_strecke (0) << endl;
         break;
      }

      default:     //mehrere Rennen wurden gefunden
      {
         cout  << left << " _________________________________________________________________________________________________________________________________________________________________________" << endl
               << left << "| " << "      Startzeit       |" << setw (20) << "  Maximalgeschwindigkeit [mph]  |  " << setw (39) << "Durchschnittsgeschwindigkeit [mph]  |  " << setw (22) << "Maximaler Schaden  |  "
               << setw (25) << "Dauer des Rennens [s]  |  " << setw (22) << "    Rennstrecke        |" << endl
               << "|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|" << endl;

         for (int i = 0; i < nAnz_rennen; i++)
         {
            cout  << left << "| " << data.gib_startzeit (i) << "  |  "  << setw (29) << data.gib_max_v (i) << setw (18) <<  " |  " << setw (20) << data.gib_durchschnitt_v (i) << setw (12) <<"  |  " << setw (10)  << data.gib_max_schaden (i) << setw (12) << "  |  "
                  << setw (14) << data.gib_dauer (i) << "  |  " << setw (22) << data.gib_strecke (i) <<  " |    " << "( " << nAnz_rennen-i << " )" << endl;
         }
         cout  << endl << nAnz_rennen << " Rennen gefunden" << endl;
         break;
      }
   }
}


/**
* @brief Ausgabe des Startbildschirms
* @param keine
* @return keine
**/
void Ausgabe::startbildschirm () const
{
   system ("clear");
   cout  << endl
         << endl
         << "     *******************************************************" << endl
         << "     | Analysetool zum Auswerten der Renndaten von Robot32 |" << endl
         << "     |                    Version 1.0                      |" << endl
         << "     | Entwickelt von: Alexander Denner, Benjamin Haas     |" << endl
         << "     *******************************************************" << endl << endl;
}


/**
* @brief Herstellung der Datenbankverbindung
* @param  keine
* @return 1: Verbindungsversuch fehlgeschlagen, 0: Verbindung erfolgreich
**/
bool Ausgabe::verbindung_datenbank ()
{
   //Verbindung mit Datenbank herstellen und prüfen ob erfolgreich, sonst Abbruch
   mysqlpp::Connection conn (false);
   if (!conn.connect ("rars", "localhost", "rars", "r2a0c1e4"))
   {
      return 1;
   }
   data.setze_connection (conn);     //Connection-Objekt an Klasse "Daten" übergeben
   return 0;
}
