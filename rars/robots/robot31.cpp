/**
 * Vorlage für den Robot im Software-Projekt
 *
 * @author    Ingo Haschler <lehre@ingohaschler.de>
 * @version   et12
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include "car.h"
#include <mysql++.h>
#include "track.h"   //für zusätzliche Streckeninformationen
//--------------------------------------------------------------------------
//                           D E F I N E S
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//                           Class Robot31
//--------------------------------------------------------------------------

class Robot31 : public Driver
{
public:
   // Konstruktor
   Robot31()
   {
      // Der Name des Robots
      m_sName = "Robot31";
      // Namen der Autoren
      m_sAuthor = "DennerAl";
      // Hier die vorgegebenen Farben eintragen
      m_iNoseColor = oBLUE;
      m_iTailColor = oBLUE;
      m_sBitmapName2D = "car_blue_blue";
      // Für alle Gruppen gleich
      m_sModel3D = "futura";
      trace_mod = 1;
   }

private:

   double vc;
   double alpha;
   double speed;
   double rad;
   bool trace_mod; //Bit zur Aktivierung der Datenbankfunktion

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//Beschreibung: Errechnung der maximalen Kurvengeschwindigkeit
//Funktionsparameter:
//    in: Kurvenradius (radius)
//Rückgabewert: max. Kurvengeschwindigkeit

   double geschw_kurve (double radius)
   {
      if (radius < 0) //Betrag des Radius bilden
      {
         rad = radius * -1;
      }
      else
      {
         rad = radius;
      }
      if (radius == 0)  //wenn Radius=0
      {
         return 200.0;  //...Vollgas
      }
      return sqrt (0.9 * 32.2 * rad); //v_max = sqrt(Reibungskoeff. * g * r)
   }



//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//Beschreibung: Errechnung des Bremsweges
//Funktionsparameter:
//    in: aktuelle Geschwindigkeit (v0)
//    in: gewünschte Geschwindigkeit vor Kurveneintritt
//Rückgabewert: benötigter Bremsweg

   double bremsweg (double v0, double v1, double a_max)
   {
      return (v0 * (v1 - v0)) / a_max + 0.5 * ((v1 - v0) * (v1 - v0)) / a_max;
   }



//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//Beschreibung: Werte in Datenbank schreiben
//Funktionsparameter:
//    in: situation s des Frameworks
//Rückgabewert: nicht vorhanden

   void schreibe_datenbank(situation &s)
   {
      struct track_desc Trackinfo = get_track_description(); //Einlesen der Streckeninformationen
      mysqlpp::Connection conn (false);
      conn.connect ("rars", "localhost", "rars", "r2a0c1e4"); //Verbindung aufbauen, Anmeldedaten aus rarsdb.sql

      mysqlpp::Query query = conn.query();
      query << "INSERT INTO tmtrace (robot, starttime, racetime, laps, velocity, fuel, damage, userdata) VALUES (";
      query << mysqlpp::quote << m_sName << ","    //Robotname
      << s.start_time << ","  //Startzeit
      << s.time_count << ","  //aktuelle Laufzeit des Rennens
      << s.laps_done << ","   //durchfahrene Runden
      << s.v << ","           //aktuelle Geschwindigkeit
      << s.fuel << ","        //aktuelle Kraftstoffrestmenge
      << s.damage << ","      //aktueller Schaden
      << mysqlpp::quote << Trackinfo.sName << ")"; //Streckenname

      query.execute();  //nötig, da INSERT nichts zurückgibt
   }

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Main - Methode

   con_vec drive (situation &s)
   {
      con_vec result;

      if(trace_mod == 1)  //Werte in Datenbank schreiben, falls Tracemode aktiv
      {
         schreibe_datenbank (s);
      }

      if (s.starting)  //beim Start: voll auftanken
      {
         result.fuel_amount = MAX_FUEL;
      }

      if (s.cur_rad == 0.0) //Robot aktuell auf Gerade
      {
         speed = geschw_kurve (s.nex_rad); //max. Kurvengeschwindigkeit

         if (s.to_end > bremsweg (s.v, speed, -30.0)) //Bremsen noch nicht erforderlich
         {
            vc = s.v + 500.0; //...Vollgas
         }
         else //Geschwindigkeit anpassen = bremsen
         {
            if (s.v > 1.02 * speed) //wenn aktuelle Geschw. zu hoch
            {
               vc = 0.9 * s.v; //...abbremsen
            }
         }
      }

      else  //Robot aktuell in Kurve
      {
         speed = geschw_kurve (s.nex_rad);
         vc = speed; //max. Kurvengeschwindigkeit beibehalten
      }


      alpha = 0.4 * (s.to_lft - 30) - 3 * s.vn; //konstanter Abstand zum linken Fahrbahnrand


      result.vc = vc;
      result.alpha = alpha;
      return result; //Rückgabe Geschw. und Lenkwinkel
   }
};
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Driver * getRobot31Instance()
{
   return new Robot31();
}
