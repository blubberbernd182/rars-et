/**
* @file robot32.cpp
* @brief Diese Datei enthält den Code für die Steuerng des Rennautos.
* @author Alexander Denner, Benjamin Haas
* @date 18.12.2013
**/
//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------
//Externe Header
#include <math.h>
#include <stdlib.h>
#include <mysql++.h>
#include <string>
#include <fstream>
//RARS-Header
#include "car.h"
#include "track.h"
#include "globals.h"
//--------------------------------------------------------------------------
//                           Class Robot32
//--------------------------------------------------------------------------
/**
* @brief Robot: Diese Klasse enthält den Code von Robot32.
**/
class Robot32 : public Driver
{
public:
//--------------------------------------------------------------------------
//                           Konstruktor
//--------------------------------------------------------------------------
   /**
   * @brief Konstruktor der Klasse Robot32. Weist dem Auto Name und Farben zu.
   **/
   Robot32()
   {
      // Der Name des Robots
      m_sName = "Robot32";
      // Namen der Autoren
      m_sAuthor = "Benjamin Haas";
      // Hier die vorgegebenen Farben eintragen
      m_iNoseColor = oCYAN;
      m_iTailColor = oRED;
      m_sBitmapName2D = "car_green_red";
      // Für alle Gruppen gleich
      m_sModel3D = "futura";
      bTRACE_MODE = 0;
   }
private:
//--------------------------------------------------------------------------
//                           Konstanten
//--------------------------------------------------------------------------
   static const double s_dErdbeschleunigung = 32.2;     /**< wichtig für maximale Kurvengeschwindigkeit */
   static const double s_dA_BREMS_GERADE = -30.0;     /**< Maximale Bremsbeschleunigung auf einer Geraden */
   static const double s_dA_BREMS_KURVE = -20.0;     /**< Maximale Bremsbeschleunigung in einer Kurve */
   static const long s_nMAX_DAMAGE = 20000.0;     /**< Schaden bei dessen Ueberschreitung Boxenstopp angefordert wird */
   static const double s_dMIN_FUEL = 20.0;     /**< Benzinfuellstand bei dessen Unterschreitung Boxenstopp angefordert wird */
   static const double s_dDIST = 5.0;     /**< Gewünschter Abstand vom linken Fahrbahnrand */
   static const double s_dAlpha_MAX = 1.0;     /**< Maximaler Lenkwinkel */
//--------------------------------------------------------------------------
//                           Aufzählungstypen
//-------------------------------------------------------------------------
   enum Streckensegmente
   {
      GERADE, LINKSKURVE, RECHTSKURVE, DOPPELKURVE_L, DOPPELKURVE_R
   };     /**< Dieser Aufzählungstyp enthält die verschiedenen Streckensegmente die vorkommen können */
//--------------------------------------------------------------------------
//                           Attribute
//--------------------------------------------------------------------------
   Streckensegmente Verlauf;     /**< Enthält den aktuellen Streckenverlauf */
   double dAlpha;     /**< Lenkwinkel, der an das Framework gegeben wird */
   double dV;     /**< Geschwindigkeit, die an das Framework gegeben wird */
   double dBreite;     /**< Breite der Strecke */
   double dSpur;     /**< Abstand den das Auto vom linken Fahrbahnrand haben soll */

   //streckenabhängige Parameter:
   double dLenk_konstante;     /**< streckenabgängige Lenkkonstante */
   double dReibungskoeffizient;     /** < streckenabhängige Reibungskonstante */

   bool bTRACE_MODE;     /**< An-/Ausschalten des TRACE-Mode */
   mysqlpp::Connection conn;     /**< Connection-Objekt */
   struct track_desc Trackinfo;     /**< zum Einlesen von Streckeninformationen */



//--------------------------------------------------------------------------
//                          Methoden
//--------------------------------------------------------------------------
public:

   /**
   * @brief Funktion vom RARS-Framework. Hier steht die eigentliche Fahrlogik.
   * @param [in] situation &p_S: Die aktuelle Situation die vom Framework geliefert wird.
   * @return Die Parameter die vom Robot an das Framework zurückgegeben werden.
   **/
   con_vec drive (situation &p_S)
   {
      con_vec result = CON_VEC_EMPTY;     //Initialisierung des Result-Structs

      if (p_S.starting)
      {
         Trackinfo = get_track_description();     //Streckeninformationen einlesen
         std::ofstream log ("log_robot32.txt", std::ios::out);     //Inhalt der Logdatei löschen
         if (!conn.connect ("rars", "localhost", "rars", "r2a0c1e4"))     //Verbindung aufbauen, Anmeldedaten aus rarsdb.sql
         {
            std::ofstream log ("log_robot32.txt", std::ios::app);     //Logdatei öffnen
            log << "Datenbankzugriff fehlgeschlagen.\n";     //Fehlermeldung
            bTRACE_MODE = 0;
         }
         initialisierung (p_S.to_lft);     //Initialisierung der Klassenvariablen

         result.fuel_amount = MAX_FUEL;     //Tankfüllung beim Start des Rennens
      }

      if (stuck(p_S.backward, p_S.v, p_S.vn, p_S.to_lft, p_S.to_rgt, &result.alpha, &result.vc))
         return result;     //Funktion vom RARS-Framework, die Auto zurück auf Strecke führt wenns rausfliegt

      streckenverlauf (p_S.cur_rad, p_S.nex_rad, p_S.after_rad);     //Attribut Verlauf beschreiben

      berechne_spur (p_S.cur_len, p_S.to_end);     //Abstand nach links berechnen
      if(!kollisionsvermeidung (p_S.nearby, p_S.v, p_S.to_lft))     //Kollisionen mit anderen Autos vermeiden
            berechne_v (p_S.cur_rad, p_S.nex_rad, p_S.to_end, p_S.v);     //Geschwindigkeit berechnen falls Kollisionsvermeidung sie nicht verändert hat
      lenkwinkel (dSpur, p_S.to_lft, p_S.vn, p_S.v);     //Lenkwinkel berechnen

      result.vc = dV;     //Geschwindigkeit an Framework zurückgeben
      result.alpha = dAlpha;     //Lenkwinkel an Framework zurückgeben

      pitstop (p_S.fuel, p_S.damage, p_S.laps_to_go, result.request_pit, result.repair_amount, result.fuel_amount); //Boxenstopps

      if (bTRACE_MODE)
      {
         trace (p_S);     //Renndaten in Datenbank schreiben
      }

      //std::ofstream log ("log_robot32.txt",std::ios::app); //Logdatei für Testzwecke

      return result;     //Werte an Framework zurückgeben.
   }

private:
   /**
   * @brief Initialisiert die Klassenvariablen.
   * @param [in] double dStartabstand: Abstand vom linken Fahrbahnrand beim Start des Rennens.
   * @return keine
   **/
   void initialisierung (double p_dStartabstand)
   {
      //Streckenabhänige Parameter beschreiben
      std::string sStreckenname = Trackinfo.sName;     // String der den Streckenname enthält

      if (!sStreckenname.compare ("hock.trk"))
      {
         switch (args.m_iSurface)     //args ist eine globale Klasse des RARS-Framework. m_iSurface ist ein Public-Attribut, welches die gewählte Surface enthält.
         {
         case 0:     //schlechteste Oberfläche
         {
            dReibungskoeffizient = 1.02;
            dLenk_konstante = 0.65;
            break;
         }
         case 1:     //mittlere Oberfläche
         {
            dReibungskoeffizient = 1.15;
            dLenk_konstante = 0.9;
            break;
         }
         case 2:     //beste Oberfläche
         {
            dReibungskoeffizient = 1.60;
            dLenk_konstante = 0.8;
            break;
         }
         }
      }
      else if (!sStreckenname.compare ("silverst.trk"))
      {
         switch (args.m_iSurface)
         {
         case 0:     //schlechteste Oberfläche
         {
            dReibungskoeffizient = 0.85;
            dLenk_konstante = 1.2;

            break;
         }
         case 1:     //mittlere Oberfläche
         {
            dReibungskoeffizient = 1.15;
            dLenk_konstante = 2.0;
            break;
         }
         case 2:     //beste Oberfläche
         {
            dReibungskoeffizient = 1.40;
            dLenk_konstante = 0.50;
            break;
         }
         }
      }
      else if (!sStreckenname.compare ("adelaide.trk"))
      {
         switch (args.m_iSurface)
         {
         case 0:     //schlechteste Oberfläche
         {
            dReibungskoeffizient = 1.05;
            dLenk_konstante = 1.6;
            break;
         }
         case 1:     //mittlere Oberfläche
         {
            dReibungskoeffizient = 1.25;
            dLenk_konstante = 1.3;
            break;
         }
         case 2:     //beste Oberfläche
         {
            dReibungskoeffizient = 1.50;
            dLenk_konstante = 0.65;
            break;
         }
         }
      }
      else if (!sStreckenname.compare ("brazil.trk"))
      {
         switch (args.m_iSurface)
         {
         case 0:     //schlechteste Oberfläche
         {
            dReibungskoeffizient = 1.00;
            dLenk_konstante = 2.50;
            break;
         }
         case 1:     //mittlere Oberfläche
         {
            dReibungskoeffizient = 1.12;
            dLenk_konstante = 2.3;
            break;
         }
         case 2:     //beste Oberfläche
         {
            dReibungskoeffizient = 1.50;
            dLenk_konstante = 2.0;
            break;
         }
         }
      }
      else if (!sStreckenname.compare ("imola.trk"))
      {
         switch (args.m_iSurface)
         {
         case 0:     //schlechteste Oberfläche
         {
            dReibungskoeffizient = 1.20;
            dLenk_konstante = 2.5;
            break;
         }
         case 1:     //mittlere Oberfläche
         {
            dReibungskoeffizient = 1.25;
            dLenk_konstante = 2.1;
            break;
         }
         case 2:     //beste Oberfläche
         {
            dReibungskoeffizient = 1.70;
            dLenk_konstante = 2.0;
            break;
         }
         }
      }
      else     //Auf anderen Strecken alle Parameter auf eins setzen
      {
         dLenk_konstante = 1;
         dReibungskoeffizient = 1;
      }

      //restliche Attribute mit Anfangswerten belegen
      dBreite = Trackinfo.width;
      dSpur = p_dStartabstand;
      Verlauf = GERADE;
      dAlpha = 0.0;
      dV = 500.0;
   }

   /**
   * @brief Beschreibt das Attribut Verlauf mit der aktuellen Kurvensituation.
   * @param [in] double p_dCurRad: Der Radius des momentanen Streckensegments.
   * @param [in] double p_dNextRad: Der Radius des nächsten Streckensegments.
   * @param [in] double p_dAfterRad: Der Radius des übernächsten Streckensegments.
   * @return keine
   **/
   void streckenverlauf (double p_dCurRad, double p_dNextRad, double p_dAfterRad)
   {
      if (p_dCurRad== 0 || fabs(p_dCurRad) > 2000)     //Auto befindet sich auf einer Geraden. Bei Betrag(Radius) > 2000 wird Kurve als Gerade angenommen.
      {
         Verlauf = GERADE;
      }
      else if (p_dCurRad > 0)     //Linkskurve
      {
         if (p_dNextRad > 0 || (p_dNextRad = 0 && p_dAfterRad > 0))
            Verlauf = DOPPELKURVE_L;
         else
            Verlauf = LINKSKURVE;
      }
      else if (p_dCurRad < 0)     //Rechtskurve
      {
         if (p_dNextRad < 0 || (p_dNextRad = 0 && p_dAfterRad < 0))
            Verlauf = DOPPELKURVE_R;
         else
            Verlauf = RECHTSKURVE;
      }
   }
   /**
   * @brief Berechnet den Abstand vom Fahrbahnrand als Funktion von p_dRest wenn sich das Auto vor dem Kurvenscheitel befindet.
   * @param [in] double p_dLaenge: Die Gesamtlänge des momentanen Streckensegments.
   * @param [in] double p_dRest: Die restliche Länge des momentanen Steckensegments.
   * @return gewünschter Abstand vom Fahrbahnrand.
   **/
   double kurve1 (double p_dLaenge, double p_dRest) const
   {
      return ((dBreite - 2 * s_dDIST) / p_dLaenge) * p_dRest - 0.5 * dBreite + 2* s_dDIST;     //siehe technische Doku
   }

   /**
   * @brief Berechnet den Abstand vom Fahrbahnrand als Funktion von p_dRest wenn sich das Auto nach dem Kurvenscheitel befindet.
   * @param [in] double p_dLaenge: Die Gesamtlänge des momentanen Streckensegments.
   * @param [in] double p_dRest: Die restliche Länge des momentanen Steckensegments.
   * @return gewünschter Abstand vom Fahrbahnrand.
   **/
   double kurve2 (double p_dLaenge, double p_dRest) const
   {
      return ((-dBreite + 2 * s_dDIST) / p_dLaenge) * p_dRest + dBreite / 2;     //siehe technische Doku
   }

   /**
   * @brief Berechnet den Abstand vom linken Rand abhängig von der aktuellen Situation und speichert ihn in dem Attribut spur ab.
   * @param [in] double p_dLaenge: Die Gesamtlänge des momentanen Streckensegments.
   * @param [in] double p_dRest: Die restliche Länge des momentanen Steckensegments.
   * @return keine
   **/
   void berechne_spur (double p_dLaenge, double p_dRest)
   {
      static bool s_bDoppelk = 0;     //Ist Eins wenn beim letzten Funktionsaufruf eine Doppelkurve vorhanden war.

      switch (Verlauf)
      {
      case GERADE:
      {
         dSpur = dBreite / 2;
         break;
      }
      case LINKSKURVE:
      {
         if (p_dRest > p_dLaenge / 2)     //Auto befindet sich vor Kurvenscheitel
            if (s_bDoppelk)     //Auto befindet sich in 2. Teil von Doppelkurve
               dSpur = s_dDIST;     //Am Streckenrand bleiben
            else
               dSpur = kurve1 (p_dLaenge, p_dRest);     //An Kurvenscheitel anfahren
         else     //Auto befindet sich nach Kurvenscheitel
            dSpur = kurve2 (p_dLaenge, p_dRest);     //In Streckenmitte fahren
         break;
      }
      case RECHTSKURVE:
      {
         if (p_dRest > p_dLaenge / 2)     //Auto befindet sich vor Kurvenscheitel
            if (s_bDoppelk)     //Auto befindet sich in 2. Teil von Doppelkurve
               dSpur = s_dDIST;     //Am Streckenrand bleiben
            else
               dSpur = dBreite - kurve1 (p_dLaenge, p_dRest);     //An Kurvenscheitel anfahren
         else     //Auto befindet sich nach Kurvenscheitel
            dSpur = dBreite - kurve2 (p_dLaenge, p_dRest);     //In Streckenmitte fahren
         break;
      }
      case DOPPELKURVE_L:     //Momentane und nächste Kurve geht nach links
      {
         if (p_dRest > p_dLaenge / 2)     //Auto befindet sich vor Kurvenscheitel
            dSpur = kurve1 (p_dLaenge, p_dRest);     //An Kurvenscheitel anfahren
         else     //Auto befindet sich nach Kurvenscheitel
            dSpur = s_dDIST;     //Am Streckenrand bleiben
         break;
      }
      case DOPPELKURVE_R:     //Momentane und nächste Kurve geht nach rechts
      {
         if (p_dRest > p_dLaenge / 2)     //Auto befindet sich vor Kurvenscheitel
            dSpur = dBreite - kurve1 (p_dLaenge, p_dRest);     //An Kurvenscheitel anfahren
         else     //Auto befindet sich nach Kurvenscheitel
            dSpur = dBreite - s_dDIST;     //Am Streckenrand bleiben
         break;
      }
      default:
      {
         dSpur = dBreite / 2;     //Im Zweifelsfall in Streckenmitte fahren
         break;
      }
      }

      if (Verlauf == DOPPELKURVE_L || Verlauf == DOPPELKURVE_R) //Wenn diese Kurve eine Doppelkurve war, dann die Variable s_bDoppel auf 1 setzen.
         s_bDoppelk = 1;
      else
         s_bDoppelk = 0;
   }

   /**
   * @brief Berechnet die neue Geschwindigkeit [Fuß/s] des Robots abhängig davon wo er sich befindet und wie der weitere Streckenverlauf aussieht.
   *        Die Geschwindigkeit wird in dem Attribut dV abgespeichert.
   * @param [in] double p_dCurRad: Der Radius des momentanen Streckensegments.
   * @param [in] double p_dNextRad: Der Radius des nächsten Streckensegments.
   * @param [in] double p_dToEnd: Weg bis zum Ende des momentanen Streckensegments [Fuß].
   * @param [in] double p_dV: momentane Geschwindigkeit.
   * @return keine
   **/
   void berechne_v (double p_dCurRad, double p_dNextRad, double p_dToEnd, double p_dV)
   {
      double v_seg = v_kurve (p_dNextRad);     //Maximale Geschwindigkeit auf nächstem Streckensegment

      if (Verlauf == GERADE)     //Das Auto befindet sich momentan auf einer Geraden
      {
         if (p_dToEnd <= bremsweg(p_dV, v_seg, s_dA_BREMS_GERADE))
            dV = v_seg;     //Geschwindigkeit an Kurve anpassen
         else
            dV = 500;     //Gas geben!
      }
      else     //Das Auto befindet sich momentan in einer Kurve
      {
         if ((p_dToEnd * p_dCurRad) <= bremsweg(p_dV, v_seg, s_dA_BREMS_KURVE))
            dV = v_seg;     //Geschwindigkeit an nächstes Segment anpassen
         else
            dV = v_kurve (p_dCurRad);     // v = Maximale Geschwindigkeit der aktuellen Kurve
      }
   }

   /**
   * @brief Berechnet die maximale Geschwindigkeit auf einem Streckensegment abhängig von dessen Radius,
   *        Die Ḱonstante s_dDIST wird innerhalb der Funktion zum Radius dazuaddiert.
   * @param [in] double p_dRadius: Der Radius des Segments.
   * @return Die maximale Geschwindigkeit [Fuß/s].
   **/
   double v_kurve (double p_dRadius) const
   {
      if (fabs (p_dRadius) < 0.001)     //Bei Radius = 0 würde v = 0 rauskommen. 0.001 wegen numerischer Ungenauigkeit.
         return 500;     //Vollgas!
      return sqrt ((fabs (p_dRadius) + s_dDIST) * s_dErdbeschleunigung) * dReibungskoeffizient;     //siehe technische Doku
   }

   /**
   * @brief Berechnet den minimalen Bremsweg um von v0 auf v1 abzubremsen mit der Bremsbeschleunigung a.
   * @param [in] double p_dV0: Anfangsgeschwindigkeit [Fuß/s].
   * @param [in] double p_dV1: Geschwindigkeit die durch das Bremsen erreicht werden soll [Fuß/s].
   * @param [in] double p_dA: Bremsbeschleunigung mit der gebremst wird [Fuß/s²].
   * @return Der Bremsweg [Fuß].
   **/
   double bremsweg (double p_dV0, double p_dV1, double p_A) const
   {
      if (p_dV1 > p_dV0)
         return 0.0;     //Wenn v1 > v0 muss nicht gebremst werden
      return (p_dV0 + .5 * (p_dV1 - p_dV0)) * (p_dV1 - p_dV0) / p_A;     //siehe technische Doku
   }

   /**
   * @brief Berechnet den momentanen Winkel zwischen vn und v.
   * @param [in] double p_dVn: Der Normalanteil der aktuellen Geschwindigkeit (orthogonal zum Streckenverlauf).
   * @param [in] double p_dV: Die aktuelle Geschwindigkeit.
   * @return Der momentane Lenkwinkel [Bogenmaß].
   **/
   double winkel_momentan (double p_dVn, double p_dV) const
   {
      return asin (p_dVn / p_dV);     //siehe technische Doku
   }

   /**
   * @brief Berechnet den neuen Lenkwinkel abhängig vom aktuellen und dem gewünschten Abstand vom linken Rand und der momentanen Geschwindigkeit.
   *        Der berechnete Winkel wird in dem Attribut dAlpha abgespeichert.
   * @param [in] double spur: Der gewünschte Abstand vom linken Rand [Fuß].
   * @param [in] double p_dAbstand_links: Der momentane Abstand vom linken Rand.
   * @param [in] double p_dVn: Der Normalanteil der aktuellen Geschwindigkeit (orthogonal zum Streckenverlauf).
   * @param [in] double p_dV: Die aktuelle Geschwindigkeit.
   * @return keine
   **/
   void lenkwinkel (double p_dSpur, double p_dAbstand_links, double p_dVn, double p_dV)
   {
      double dALpha1 = dLenk_konstante * (p_dAbstand_links - p_dSpur) * 0.01;     //siehe technische Doku
      double dAlpha2 = winkel_momentan (p_dVn, p_dV);
      dAlpha = dALpha1 - dAlpha2;     //Zurückgegebener Winkel = Differenz zwischen Soll- und Istwert

      //Zu hohe Lenkwinkel verhindern
      if (dAlpha > s_dAlpha_MAX)
         dAlpha = s_dAlpha_MAX;
      if (dAlpha < 0.0 - (s_dAlpha_MAX))
         dAlpha = 0.0 - (s_dAlpha_MAX);
   }

   /**
   * @brief Ist für Boxenstopps zuständig. Entscheidet ob ein Boxenstopp ausgeführt werden soll und führt ihn gegebenenfalls aus.
   * @param [in] double p_dBenzin: Momentane Tankfüllung.
   * @param [in] long p_Schaden: Momentaner Schaden.
   * @param [in] int p_nRest_Runden: Anzahl verbleibender Runden bis zum Ende des Rennens.
   * @param [out] int &nRequest: Wird auf eins gesetzt wenn Boxenstopp angefordert wird.
   * @param [out] int &nRepAmput: Wird mit der Menge an Schadenspunkten die repariert werden soll beschrieben.
   * @param [out] double &dFuelAmount: Wird mit der Menge an Benzin die nachgetankt werden soll beschrieben.
   * @return 1: Boxenstopp, 0: kein Boxenstopp
   **/
   bool pitstop (double p_dBenzin, long p_Schaden, int p_nRest_Runden, int &nRequest, int &nRepAmount, double &dFuelAmount) const
   {
      if ((p_dBenzin < s_dMIN_FUEL || p_Schaden > s_nMAX_DAMAGE) && p_nRest_Runden != 1)
      {
         nRequest = 1;     //Boxenstopp anfordern
         nRepAmount = p_Schaden;     //Menge an Schadenspunkten die repariert werden soll festlegen
         dFuelAmount = MAX_FUEL;     //Menge an Benzin die nachgetankt werden soll festlegen
         return 1;
      }
      else
         return 0;
   }

   /**
   * @brief Rechnet fps in mph um
   * @param [in] p_dVfps: Geschwindigkeit in fps
   * @return Geschwindigkeit in mph
   **/
   double fpstomph (double p_dVfps) const
   {
      const double dUMRECHNUNGSFAKTOR = 0.68182;     //Umrechnungsfaktor von fps nach mph
      return p_dVfps * dUMRECHNUNGSFAKTOR;     //Zurückgeben der Geschwindigkeit in mph
   }

   /**
   * @brief Schreibt bestimmte Informationen über das Rennen in eine Datenbank.
   *        Schlägt die Datenbankverbindung fehl, wird jedes mal eine Fehlermeldung in die Logdatei "log_robot32.txt" geschrieben.
   * @param [in] situation &p_S: Die aktuelle Situation die vom Framework geliefert wird.
   * @return keine
   **/
   void trace (situation &p_S)
   {


      mysqlpp::Query query = conn.query ();
      query << "INSERT INTO tmtrace (robot, starttime, racetime, laps, velocity, fuel, damage, userdata) VALUES (";
      query << mysqlpp::quote << m_sName << ","     //Name des Robots
      << p_S.start_time << ","     //Startzeit des Rennens
      << p_S.time_count << ","     //Vergangene Zeit seit Start des Rennens
      << p_S.laps_done << ","     //Gefahrene Runden
      << fpstomph (p_S.v) << ","     //Momentane Geschwindigkeit in mph
      << p_S.fuel << ","     //Momentane Menge an Benzin
      << p_S.damage << ","     //Momentaner Schaden
      << mysqlpp::quote << Trackinfo.sName << ")";     //Name der Rennstrecke
      query.execute ();     //Ausführen des INSERT-Befehls
   }

   /**
   * @brief Diese Methode soll Kollisionen vermeiden. Wenn sich Fahrzeuge in der Nähe befinden passt sie Geschwindigkeit und gewünschten Abstand vom Rand an.
   * @param [in] rel_state p_Status[3]: Array das den Status der Fahrzeuge in der Nähe anzeigt
   * @param [in] double p_dV: Momentane Geschwindigkeit des Autos
   * @param [in] double p_dAbstand_Links: Momentaner Abstand vom linken Streckenrand
   * @return 0: Das Attribut dV wurde durch die Funktion nicht verändert. 1: Das Attribut dV wurde verändert
   **/
   bool kollisionsvermeidung (rel_state p_Status[3], double p_dV, double p_dAbstand_Links)
   {
      const double dABSTAND = CARLEN * 3.0;     //Abstand bei dem das nächste Auto als "nah" betrachtet wird
      const double dSPURAENDERUNG = 3.0;     //Änderung des Attributs Spur für den Überholvorgang

      if (p_Status[0].who > 15)     //Es befindet sich kein Auto in der Nähe vor uns
         return 0;

      //Wenn mehr als ein Auto vor uns ist bremsen
      if (p_Status[1].who > 15)
      {
         dV = p_dV * 0.95;     //Bremsen!
         return 1;
      }

      double dX = p_Status[0].rel_x;     //Abstand nach vorne zum nächsten Auto
      double dY = p_Status[0].rel_y;     //Abstand nach rechts zum nächsten Auto
      double dVx = p_Status[0].rel_xdot;     //Geschwindigkeit des nächsten Autos in x-Richtung relativ zu uns
      double dVy = p_Status[0].rel_ydot;     //Geschwindigkeit des nächsten Autos in y-Richtung relativ zu uns

      double dSkalarprod = (dX * dVx + dY * dVy);     //Skalarprodukt von Entferungsvektor und relativem Geschwindigkeitsvektor des nächsten Autos

      if (dSkalarprod > 0)     //Das nächste Auto bewegt sich momentan von uns weg
         return 0;

      double dAbstand = sqrt(dX * dX + dY * dY);     //Abstand von uns zum nächsten Auto
      double dVrel = sqrt (dVx * dVx + dVy * dVy);     //Betrag des Geschwindigkeitsvektors des nächsten Autos relativ zu uns
      double dT = dAbstand / dVrel;     //Zeit bis zu größter Annäherung zum nächsten Auto

      if (dAbstand > dABSTAND)     //Das nächste Auto ist noch weit genug entfernt
         return 0;

      //Genauere Berechnung ob es zu einem Crash kommen würde wenn nicht reagiert wird
      dX = dX + dT * dVx;     //X-Koordinate des nächsten Autos zum Zeitpunkt der größten Annäherung
      dY = dY + dT * dVy;     //Y-Koordinate des nächsten Autos zum Zeitpunkt der größten Annäherung

      if (fabs(dX) > CARWID * 1.50 && fabs(dY) > 1.50 * CARLEN)
         return 0;

      //Wenn der Algorithmus hier angelangt ist würde es wahrscheinlich zu einem Crash kommen wenn nicht reagiert wird.

      if (dX >= 0.0)     //Auto befindet sich rechts von uns
      {
         if (dSpur > 10)     //Ist Links genug Platz zum Überholen?
         {
            dSpur = p_dAbstand_Links - dSPURAENDERUNG;
            return 0;
         }
      }
      else     //Auto befindet sich links von uns
      {
         if ((dBreite - dSpur) > 10)     //Ist Rechts genug Platz zum Überholen?
         {
            dSpur = p_dAbstand_Links + dSPURAENDERUNG;
            return 0;
         }
      }

      dV = p_dV * 0.95;     //Bremsen!
      return 1;
   }
};

/**
 * @brief Diese Methode darf nicht verändert werden.
 *        Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 *        Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 * @return Robot32
 **/
Driver * getRobot32Instance ()
{
   return new Robot32 ();
}
