/**
* @file main.cpp
* @brief Hauptprogramm
* @date 07.12.2013
* @author Alexander Denner, Benjamin Haas
**/
#include "Ausgabe.h"

int main ()
{
   Ausgabe ausgabe;
   ausgabe.hauptmenue ();

   return 0;
}
