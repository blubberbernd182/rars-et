/**
* @file Daten.cpp
* @brief Implementierungsdatei der Klasse "Daten"
* @date 04.03.2014
* @author Alexander Denner, Benjamin Haas
**/
#include "Daten.h"
using namespace std;


/**
* @brief Konstruktor von "Daten"
* @param keine
* @return keine
**/
Daten::Daten ()
{
   nAnz_reihen = 0;
   nAnz_rennen = 0;
   nAnz_runden = 0;
   nStart_naechste = 0;
   nStartid.clear ();
   nEndid.clear ();
}


/**
* @brief Erfassung der Dauer eines Rennens
* @param [in] p_nIndex Index des Rennbereichs in der Datenbank
* @return maximale Renndauer
**/
double Daten::gib_dauer (int p_nIndex)
{
   mysqlpp::Query query = conn.query ();
   query << "SELECT MAX(racetime) AS t_max FROM tmtrace WHERE robot = " << mysqlpp::quote << sName << " AND id BETWEEN " << nStartid [p_nIndex]  << " AND " << nEndid [p_nIndex] << ";";
   res = query.store ();
   return res[0]["t_max"];
}


/**
* @brief Erfassung der Durchschnittsgeschwindigkeit eines Rennens
* @param [in] p_nIndex Index des Rennbereichs in der Datenbank
* @return durchschnittliche Geschwindigkeit
**/
double Daten::gib_durchschnitt_v (int p_nIndex)
{
   mysqlpp::Query query = conn.query ();
   query << "SELECT AVG(velocity) AS v_avg FROM tmtrace WHERE robot = " << mysqlpp::quote << sName << "AND id BETWEEN " << nStartid [p_nIndex]  << " AND " << nEndid [p_nIndex] << ";";
   res = query.store ();
   return res [0]["v_avg"];
}


/**
* @brief Erfassung des maximalen Schadens eines Rennens
* @param [in] p_nIndex Index des Rennbereichs in der Datenbank
* @return maximaler Schaden
**/
double Daten::gib_max_schaden (int p_nIndex)
{
   mysqlpp::Query query = conn.query ();
   query << "SELECT MAX(damage) AS d_max FROM tmtrace WHERE robot = " << mysqlpp::quote << sName << " AND id BETWEEN " << nStartid [p_nIndex]  << " AND " << nEndid [p_nIndex] << ";";
   res = query.store ();
   return res [0]["d_max"];
}


/**
* @brief Erfassung der maximalen Geschwindigkeit eines Rennens
* @param [in] p_nIndex Index des Rennbereichs in der Datenbank
* @return maximale Geschwindigkeit
**/
double Daten::gib_max_v (int p_nIndex)
{
   mysqlpp::Query query = conn.query ();
   query << "SELECT MAX(velocity) AS v_max FROM tmtrace WHERE robot = " << mysqlpp::quote << sName << "AND id BETWEEN " << nStartid [p_nIndex]  << " AND " << nEndid [p_nIndex] << ";";
   res = query.store ();
   return res [0]["v_max"];
}


/**
* @brief Rückgabe der Anzahl an Rennen
* @param keine
* @return Anzahl an Rennen, abhängig von der Auswahl im Hauptmenü ("Letztes Rennen", "Suche Strecke" oder "Alle Rennen")
**/
int Daten::gib_rennen () const
{
   return nAnz_rennen;
}


/**
* @brief Erfassung der Anzahl an Runden eines Rennens
* @param [in] p_nIndex Index des Rennbereichs in der Datenbank
* @return Anzahl
**/
int Daten::gib_runden (int p_nIndex)
{
   mysqlpp::Query query = conn.query ();
   //Anzahl der Runden des gewünschten Rennens bestimmen
   query << "SELECT MAX(laps) AS laps_max FROM tmtrace WHERE robot = " << mysqlpp::quote << sName << " AND id BETWEEN " << nStartid [p_nIndex]  << " AND " << nEndid [p_nIndex] << ";";
   res = query.store ();
   nAnz_runden = res [0]["laps_max"];
   return nAnz_runden;
}


/**
* @brief Erfassung der Startzeit eines Rennens
* @param [in] p_nIndex Index des Rennbereichs in der Datenbank
* @return Startzeit
**/
string Daten::gib_startzeit (int p_nIndex)
{
   mysqlpp::Query query = conn.query ();
   query << "SELECT tmtime FROM tmtrace WHERE robot = " << mysqlpp::quote << sName << " AND id = " << nStartid [p_nIndex] << ";";
   res = query.store ();
   sStartzeit = mysqlpp::stream2string (res [0]["tmtime"]);
   sStartzeit.insert (10, ",");
   return sStartzeit;
}


/**
* @brief Erfassung des Streckennamens für ein Rennen
* @param [in] p_nIndex Index des Rennbereichs in der Datenbank
* @param [in] p_nArt Vorbelegung = 0   0: bei "hock" und "silverst" werden vollständige Namen hinzugefuegt;  1: Kurzname der Strecke zurückgeben (verwendet bei Diagrammexport)
* @return Streckenname (ohne trk-Endung)
**/
string Daten::gib_strecke (int p_nIndex, bool p_nArt)
{
   mysqlpp::Query query = conn.query ();
   query << "SELECT userdata FROM tmtrace WHERE robot = " << mysqlpp::quote << sName << " AND id = " << nStartid [p_nIndex]  << ";";
   res = query.store ();

   sStrecke = mysqlpp::stream2string (res [0]["userdata"]);
   sStrecke.erase (sStrecke.end () - 4, sStrecke.end ());     //Löschen der letzten vier Stellen des Ergebnisstrings
   if (p_nArt)     //falls Namensergänzung nicht erwünscht
   {
      return sStrecke;
   }
   if (sStrecke == "hock")
   {
      return "hockenheim (hock)";
   }
   else
   {
      if (sStrecke == "silverst")
      {
         return "silverstone (silverst)";
      }
      else
      {
         return sStrecke;
      }
   }
   return sStrecke;
}


/**
* @brief Erfassung der Start- und Endids eines Rennbereichs
* @param [in] p_nArt Vorbelegung = 0; Unterscheidungsvariable, wie Grenzen ausgelesen werden; 1: Letztes Rennen, 2: Strecke suchen, 0: Alle Rennen
* @param [in] p_sEingabe Vorbelegung = "."; Streckennamen, nach dem gesucht werden soll
* @return keine
**/
void Daten::erfasse_grenzen (int p_nArt, string p_sEingabe)
{
   mysqlpp::Query query = conn.query ();

   nStartid.clear ();     //Elemente in nStartid[] und nEndid[] löschen
   nEndid.clear ();

   sStrecke = trk_anhaengen (p_sEingabe);     //an eingegebene Strecke (p_sEingabe) wird ".trk"-Endung angehängt

   switch (p_nArt)
   {
      case 1:     //Menüpunkt 1: Letztes Rennen
      {
         nAnz_rennen = 1;
         //Startid bestimmen
         query << "SELECT id FROM tmtrace WHERE robot = " << mysqlpp::quote << sName << " AND id = (SELECT MAX(id) FROM tmtrace WHERE racetime = 0 AND robot = " << mysqlpp::quote << sName <<");";
         res = query.store ();
         nStartid.push_back (res [0]["id"]);
         //Endid bestimmen
         query << "SELECT MAX(id) as id_max from tmtrace where robot = " << mysqlpp::quote << sName << ";";
         res = query.store ();
         nEndid.push_back (res [0]["id_max"]);
         break;
      }
      default:  //Menüpunkt 2 und 3: Suche_Strecke bzw. Alle_Rennen
      {
         //Startids bestimmen
         query << "SELECT id FROM tmtrace WHERE racetime = 0 AND robot = " << mysqlpp::quote << sName;

         if (p_nArt == 2)     //falls "Suche Strecke" ausgewählt: im SQL-Query zusätzlich nach Strecke filtern
         {
            query << " AND userdata = " << mysqlpp::quote << sStrecke;
         }
         query << " ORDER BY id DESC;";
         res_0 = query.store ();
         nAnz_reihen =  res_0.num_rows ();     //Anzahl an Startids in der Datenbank speichern


         for (size_t i = 0; i < (size_t) nAnz_reihen; i++)
         {
            nStartid.push_back (res_0 [i]["id"]);     //Startids speichern

            //nächst höhere Startid erfassen: kleinste ID in Datenbank, die größer als nStartid[] ist, sowie racetime = 0
            query << "SELECT MIN(id) AS id_min FROM tmtrace WHERE racetime = 0 AND robot = " << mysqlpp::quote << sName << " AND id > " << mysqlpp::quote << nStartid[i] << ";";
            res = query.store();
            if (res[0]["id_min"].is_null())     //Ergebnis ist leer: nach diesem Streckenbereich folgt kein neuer mehr, d.h. Endid ist die max. ID in Datenbank
            {
               query << "SELECT MAX(id) AS id_max FROM tmtrace WHERE robot = " << mysqlpp::quote << sName;
               if (p_nArt == 2)
               {
                  query << " AND userdata = " <<mysqlpp::quote << sStrecke;
               }
               res = query.store ();
               nEndid.push_back (res [0]["id_max"]);
            }
            else
            {
               nStart_naechste = res [0]["id_min"];     //nächst höhere Startid zwischenspeichern
               //Endid bestimmen: Maximale ID im Bereich (nStartid[] - nStart_naechste)
               query << "SELECT MAX(id) as id_max FROM tmtrace WHERE robot = " << mysqlpp::quote << sName << " AND id >= " << mysqlpp::quote << nStartid [i] << " AND id < " << mysqlpp::quote << nStart_naechste;
               if (p_nArt == 2)
               {
                  query << " AND userdata = " << mysqlpp::quote << sStrecke;
               }
               res = query.store ();
               nEndid.push_back (res[0]["id_max"]);
            }
         }
         nAnz_rennen = nAnz_reihen;
         break;
      }
   }
}


/**
* @brief Geschwindigkeits- und Schadenswerte auslesen und in die Zieldatei schreiben
* @param [in] p_nIndex Index des Rennbereichs in der Datenbank
* @param [in] p_nRunde Rennrunde, die exportiert werden soll
* @return -1: Datei zum Schreiben kann nicht geöffnet werden
**/
int Daten::schreibe_geschw_schaden (int p_nIndex, int p_nRunde)
{
   mysqlpp::Query query = conn.query ();

   //identische Basisabfrage für alle drei folgenden Fälle
   query << "SELECT velocity, damage FROM tmtrace WHERE robot = " << mysqlpp::quote << sName << " AND id BETWEEN " << nStartid [p_nIndex]  << " AND " << nEndid [p_nIndex];

   //Fall 1: "Nutzer: gesamter Rennverlauf soll exportiert werden"  ODER  "Rennen besteht nur aus einer nicht kompletten Runde"
   //SQL-Abfrage: wie oben (alle Datensätze im Bereich (startid - endid) auslesen)

   //Fall 2: "Nutzer: erste Runde (laps = 0) soll exportiert werden"; wichtig hierbei: es existieren Datensätze mit laps > 0
   //SQL-Abfrage: wie oben, zusätzliche Eingrenzung des laps-Bereichs
   if (p_nRunde == 1)
   {
      query << " AND laps BETWEEN -1 AND 0";
   }

   //Fall 3: "Nutzer: Runde >=2 soll exportiert werden"
   //SQL-Abfrage: identische Abfrage wie bei (p_nRunde == 0), zusätzlich laps-Eingrenzung
   if (p_nRunde > 1)
   {
      query << " AND laps = " << mysqlpp::quote << p_nRunde -1;
   }

   query << ";";
   res = query.store ();
   nAnz_reihen = res.num_rows();

   ofstream Zieldatei ("diagramm_werte.dat");
   if (!Zieldatei)     //Fehlermeldung
   {
      return -1;
   }

   bSchaden_vorhanden = 0;     //wird in der for-Schleife ggf. gesetzt, bleibt sonst gleich Null

   for (size_t i = 0; i < (size_t) nAnz_reihen; i++)     //Werte in Datei schreiben; Spalte 1: Geschwindigkeit, Spalte 2: Schaden
   {
      Zieldatei << res[i]["velocity"] << "\t\t" << res[i]["damage"] << endl;
      if (res[i]["damage"] != "0")     //ist mindestens ein Schadenswert ungleich Null, wird bSchaden_vorhanden gesetzt
      {
         bSchaden_vorhanden = 1;
      }
   }
   Zieldatei.close();

   erstelle_diagramm (gib_strecke (p_nIndex, 1), gib_startzeit (p_nIndex), p_nRunde);
   return 0;
}


/**
* @brief empfängt das Connection-Objekt aus "Ausgabe"
* @param [in,out] pConn Connection-Objekt
* @return keine
**/
void Daten::setze_connection (mysqlpp::Connection &pConn)
{
   conn = pConn;
}


/**
* @brief setzt den Namen des Robot
* @param [in,out] pConn Connection-Objekt
* @return keine
**/
void Daten::setze_name (string p_sName)
{
   sName = p_sName;
}


/**
* @brief Übermittlung der Gnuplotbefehle an die Kommandozeile zur Erstellung des Diagramms
* @param [in] p_sStreckenname Name der Rennstrecke
* @param [in] p_sStartzeit Startzeit des Rennens
* @param [in] p_nRunde Runde des Rennens
* @return keine
**/
void Daten::erstelle_diagramm (string p_sStreckenname, string p_sStartzeit, int p_nRunde)
{
   sDiagramm_Dateiname = "set output '";     //Dateiname des Diagramms erzeugen, Struktur : "set output 'DATEINAME.png'"
   sStream.str ("");     //Stringstream leeren
   sStream << p_sStreckenname << "-" << p_sStartzeit.substr (8,2) << "_" << p_sStartzeit.substr (5,2) << "_" << p_sStartzeit.substr (0,4) << "-" << p_sStartzeit.substr (12,2) << "_" << p_sStartzeit.substr (15,2);
   if (p_nRunde > 0)     //falls spezifische Runde exportiert wird, wird diese an den Stream angehängt
   {
      sStream << "-Runde" << p_nRunde;
   }
   sStream << ".png'";
   sDiagramm_Dateiname += sStream.str();

   gp_pipe = popen ("gnuplot", "w");

   gp_kommando ("set terminal png");
   gp_kommando (sDiagramm_Dateiname);     //Export als png-Datei
   gp_kommando ("set y2tics");     //zweite y-Achse (für Schaden) aktivieren
   gp_kommando ("set ytics nomirror");     //Skalierungen der ersten y-Achse werden nicht auf der zweiten angezeigt
   gp_kommando ("set ylabel 'Geschwindigkeit  /  (mph)'");
   gp_kommando ("set y2label 'Schaden'");

   //Die zweite Spalte (Schaden) von "diagramm_werte.dat" ist durchgehend Null.
   //Um die Gnuplot-Warnung "empty y2range..." zu vermeiden, wird die Skalierung der Schadensachse von -1 bis 1 vorgenommen
   if (!bSchaden_vorhanden)
   {
      gp_kommando ("set y2range [-1:1]");
   }

   gp_kommando ("plot 'diagramm_werte.dat' using 1 title 'Geschwindigkeit' axis x1y1 with lines, 'diagramm_werte.dat' using 2 title  'Schaden' axis x1y2 with lines");
   fclose (gp_pipe);

   cout << "Diagramm wurde erfolgreich exportiert" << endl;
   cin.get();
}


/**
* @brief schreibt den Befehl in die Gnuplot-Pipe
* @param [in] p_sKommando zu übermittelnder Befehl
* @return keine
**/
void Daten::gp_kommando (string p_sKommando)
{
   p_sKommando += " \n";
   fprintf (gp_pipe, "%s", p_sKommando.c_str());
   fflush (gp_pipe);
}


/**
* @brief hängt zur weiteren Verarbeitung der eingegebenen Strecke bei Menüpunkt 2 die Endung ".trk" an
* @param [in] p_nEingabe eingegebene Strecke
* @return eingegebene Strecke mit Endung ".trk"
**/
string Daten::trk_anhaengen (string p_nEingabe) const
{
   if (p_nEingabe == "hockenheim")
   {
      p_nEingabe = "hock.trk";
   }
   else
   {
      if (p_nEingabe == "silverstone")
      {
         p_nEingabe = "silverst.trk";
      }
      else
      {
         p_nEingabe += ".trk";
      }
   }
   return p_nEingabe;
}
