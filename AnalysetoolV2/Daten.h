/**
* @file Daten.h
* @brief Header der Klasse "Daten"
* @date 04.03.2014
* @author Alexander Denner, Benjamin Haas
**/
#ifndef _DATEN_H
#define _DATEN_H

#include <mysql++.h>
#include <vector>
#include <fstream>
#include <stdio.h>

using namespace std;


/**
 *  @brief Analysetool: zuständig für die Verarbeitung der Informationen aus der Datenbank
 */
class Daten
{
public:

   Daten ();

private:

   bool bSchaden_vorhanden;     /**< zur Skalierung des Diagramms auf der Schaden-Achse */
   int nAnz_reihen;     /**< beinhaltet "res.num_rows ()" des Ergebnisses eines SQL-Querys */
   int nAnz_rennen;     /**< Anzahl Grenzpaare */
   int nAnz_runden;     /**< Anzahl der Runden eines Rennens */
   unsigned int nStart_naechste;     /**< Startid des nächst höheren Rennbereichs in der Datenbank */
   vector <unsigned int> nStartid;     /**< erste ID eines Rennbereichs in der Datenbank */
   vector <unsigned int> nEndid;     /**< letzte ID eines Rennbereichs in der Datenbank */
   string sName;     /**< Name des Robots, dessen Renndaten ausgewertet werden sollen */
   string sStrecke;     /**< Streckenname */
   string sDiagramm_Dateiname;     /**< enthält Gnuplot-Befehl zur Dateibezeichnung des Diagramms */
   stringstream sStream;     /**< Stream zum Zusammenstellen des Diagramm-Dateinamens */
   string sStartzeit;     /**< enthält die formatierte Startzeit eines Rennens */
   mysqlpp::Connection conn;     /**< Connectionobjekt */
   mysqlpp::StoreQueryResult res;     /**< Ergebnis des SQL-Querys */
   mysqlpp::StoreQueryResult res_0;     /**< zweites Objekt, um doppelte for-Schleife in "erfasse_grenzen (...)" zu vermeiden */
   FILE *gp_pipe;     /**< Filestream zum Aufruf von Gnuplot */

public:

   double gib_dauer (int p_nIndex);
   double gib_durchschnitt_v (int p_nIndex);
   double gib_max_schaden (int p_nIndex);
   double gib_max_v (int p_nIndex);
   int gib_rennen () const;
   int gib_runden (int p_nIndex);
   string gib_startzeit (int p_nIndex);
   string gib_strecke (int p_nIndex, bool p_nArt = 0);

   void erfasse_grenzen (int p_nArt = 0, string p_sEingabe = ".");
   int schreibe_geschw_schaden (int p_nIndex, int p_nRunde = 0);
   void setze_connection (mysqlpp::Connection &pConn);
   void setze_name (string p_sName);

private:

   void erstelle_diagramm (string p_sStreckenname, string p_sStartzeit, int p_nRunde);
   void gp_kommando (string p_sKommando);
   string trk_anhaengen (string p_nEingabe) const;
};

#endif /*_DATEN_H*/
