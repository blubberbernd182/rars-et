/**
 * Vorlage für den Robot im Software-Projekt
 *
 * @author    Ingo Haschler <lehre@ingohaschler.de>
 * @version   et12
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include "car.h"
// parameters to tinker with:
// accelerations are in feet/second per second.
// slips are in feet/second
// distances are in feet
const double CORN_MYU   = 1.00;     // lateral g's expected when cornering
const double BRAKE_ACCEL = -33.0;   // acceleration when braking on straight
const double BRAKE_SLIP  = 6.5;     // tire slip when braking
const double BRK_CRV_ACC = -27.0;   // acceleration when braking in curve
const double BRK_CRV_SLIP = 3.5;    // tire slip for braking in curve
const double MARGIN = 10.0;         // target distance from curve's inner rail
const double MARG2 = MARGIN+10.0;   // target for entering the curve
const double ENT_SLOPE = .33;       // slope of entrance path before the curve
const double STEER_GAIN = 1.0;      // gain of steering servo
const double  DAMP_GAIN = 1.2;      // damping of steering servo
const double  BIG_SLIP = 9.0;       // affects the bias of steering servo
const double CURVE_END = 4.0;       // when you are near end of curve, widths
const double TOO_FAST = 1.02;    // a ratio to determine if speed is OK in curve
const double DELTA_LANE = 2.5;   // if collision predicted, change lane by this
//--------------------------------------------------------------------------
//                           Class Robot00
//---------------------------------------------------------------------------------

class Robot00 : public Driver
{
public:
   // Konstruktor
   Robot00()
   {
      // Der Name des Robots
      m_sName = "Robot00";
      // Namen der Autoren
      m_sAuthor = "";
      // Hier die vorgegebenen Farben eintragen
      m_iNoseColor = oBLUE;
      m_iTailColor = oBLUE;
      m_sBitmapName2D = "car_blue_blue";
      // Für alle Gruppen gleich
      m_sModel3D = "futura";
   }
   double geschw_kurve(double radius)
   {
      double rad;
      if (radius<0)
      {
         rad=radius*-1;
      }
      else     //kkk
      {
         rad=radius;
      }
      if (radius==0)
      {
         return 200.0;
      }
      return sqrt(0.9*32.2*rad);
   }


   double bremsweg (double v0, double v1, double a_max)
   {
      return (v0*(v1-v0))/a_max + 0.5*((v1-v0)*(v1-v0))/a_max;
   }

   con_vec drive(situation &s)       // This is the robot "driver" function:
   {
      con_vec result;                    // This is what is returned.
      if (s.starting)
      {

         result.fuel_amount=MAX_FUEL;
      }
      double vc, alpha, speed;



      if (s.cur_rad==0.0)     //auf Gerade
      {
         speed=geschw_kurve(s.nex_rad);
         if (s.to_end>bremsweg(s.v,speed,-30.0))
         {
            vc=s.v+500.0;
            if (1)
            {

            }


            else        //Geschwindigkeit anpassen
            {
               speed=geschw_kurve(s.nex_rad);
               if (s.v>1.02*speed)
               {
                  vc=0.9*s.v;
               }


            }

         }
         else
         {
            speed=geschw_kurve(s.nex_rad);
            vc=speed;
         }



         /*double rad;
          if (s.cur_rad<0)rad=s.cur_rad*-1;
          else rad=s.cur_rad;


          if (rad>1){
           result.vc=sqrt(rad*32.2*0.9);
          }
         -        result.alpha=0.01*(s.to_lft-s.to_rgt)-0.01*s.vn;
         -
         -
         -       return result;
         -
         +        result.alpha=0.01*(s.to_lft-s.to_rgt)-0.01*s.vn;*/

         alpha = 0.5 * (s.to_lft - 30) - 2.0 * s.vn;
         result.vc=vc;
         result.alpha=alpha;

         return result;
      }
   };

   /**
    * Diese Methode darf nicht verändert werden.
    * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
    * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
    */
   Driver * getRobot00Instance()
   {
      return new Robot00();
   }







